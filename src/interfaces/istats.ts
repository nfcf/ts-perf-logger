export interface IStats {
  count: number;
  mean: number;
  min: number;
  max: number;
  m2: number;
}
