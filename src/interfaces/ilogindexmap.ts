export interface ILogIndexMap {
  [k: string]: number;
}
