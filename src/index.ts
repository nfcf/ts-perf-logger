export * from './interfaces';
export { PerfLog } from './perflog';
export { PerfLogManager } from './perflog-manager';
export { LogClassPerformance, LogFunctionPerformance, DisableLogFunctionPerformance } from './perflog-decorators';

import './perflog-rxjs-operators';
